from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django import forms
from time import strftime, strptime
from common.postgres_handler import PostgresHandler
  
    
def index(request):
    
    context = {}
    result = []
    date_range = '01/1/2013 - 01/1/2013'
    
    if request.method == 'POST':
        try: 
            date_range = request.POST['start_end_date']
            date_split = date_range.split(' ')
            start = date_split[0]
            end = date_split[2]
            
            #convert string to time
            start_time = strptime(start, "%m/%d/%Y")
            end_time = strptime(end, "%m/%d/%Y")
                
            result = PostgresHandler().get_site_visits(start_time, end_time)
            if isinstance(result, dict) and 'error' in result:
                context ['error'] = result['error']
                result = []
                
        except Exception, e:
            #reset input for Error
            context = {}
            result = []
            date_range = '01/1/2013 - 01/1/2013' 
    
    context ['ranking'] = result
    context ['date_range'] = date_range
        
    return render(request, 'polls/ranking.html', context)

    
