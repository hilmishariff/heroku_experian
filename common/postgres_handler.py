import psycopg2
from time import strftime

       
class PostgresHandler():
    
    def __init__(self):
        try:
            self.error = ''
            #These are hardcoded. Normally it would be in  settings.py
            self.conn = psycopg2.connect("dbname='ddbmn90c2e5lbu' user='nvemriwxciatkg' host='ec2-54-204-21-178.compute-1.amazonaws.com' password='H9Fa_Zk_BP_EoYoA22rl1-zzEm'")
            self.cursor = self.conn.cursor()
        except Exception, e:
            self.error = str(e)
            
    def __del__(self):
        self.conn.close()
    
    """
    Run the SQL query
    """
    def _run_query(self, query):
        query_result = []
        try:
            self.cursor.execute(query)      
            query_result = self.cursor.fetchall()
            return query_result 

        except Exception, e:
            self.error = str(e)
            return {'error': self.error}        
            
    """
    Get the site and total visits between specified start and end dates.
    """    
    def get_site_visits(self, start, end):
        try:
            start_date = strftime('%Y-%m-%d', start)
            end_date = strftime("%Y-%m-%d", end)

            query =  "SELECT website, SUM (visits) FROM ranking_data"
            query += " WHERE (date>='%s' AND date<='%s')"        
            query += " GROUP BY website" 
            a_query = query % (start_date, end_date)
            
            website_visits = self._run_query(a_query)   
            
            if isinstance(website_visits, dict) and 'error' in website_visits:
                return website_visits
                
            result = []
            for a_result in website_visits:
                result.append({
                "website": a_result[0],
                "visits": int(a_result[1]),
                })     
            
            return result
        
        except Exception, e:
            self.error = str(e)
            return {'error': self.error}
