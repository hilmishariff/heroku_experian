from django.conf.urls import patterns, include, url
import polls

urlpatterns = patterns('',
    url(r'^$', 'polls.views.index', name='index'),
)
